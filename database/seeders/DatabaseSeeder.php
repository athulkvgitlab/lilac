<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Department;
use App\Models\Designation;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        Department::factory()
            ->count(2)
            ->has(
                User::factory()
                    ->count(2)
            )
            ->create();
        Designation::factory()
            ->count(2)
            ->has(
                User::factory()
                    ->count(2)
            )
            ->create();
    }
}
