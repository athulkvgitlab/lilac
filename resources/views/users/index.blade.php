@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="grey-box" id="userDataContainer">
                <form id="searchForm">
                    <div class="form-group">
                        <b>Search</b>
                        <input id="searchInput" type="text" class="form-control" placeholder="Search name/designation/department">
                    </div>
                </form>
                @foreach ($users->chunk(2) as $chunk)
                <div class="row fiterData">
                    @foreach ($chunk as $user)
                    <div class="col">
                        <div class="form-group white-box">
                            <b>{{ $user->name }}</b><br>
                            <b>{{ $user->designation->name }}</b><br>
                            <b>{{ $user->department->name }}</b>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $("#searchInput").on("keyup", function() {
            var value = $(this).val();
            $.ajax({
                url: "{{ route('search') }}",
                method: "GET",
                data: { value: value },
                success: function(response) {
                    $("#userDataContainer .row").not(":first").remove();
                    $('.fiterData').html('');
                    var userData = '';
                    if (response.users.length > 0) {
                        $.each(response.users, function(index, user) {
                            if (index % 2 === 0) {
                                userData += '<div class="row">';
                            }
                            userData += `<div class="col">
                                            <div class="form-group white-box">
                                                <b>${user.name}</b><br>
                                                <b>${user.designation ? user.designation.name : ''}</b><br>
                                                <b>${user.department ? user.department.name : ''}</b>
                                            </div>
                                        </div>`;
                            if ((index + 1) % 2 === 0 || (index + 1) === response.users.length) {
                                userData += '</div>';
                            }
                        });
                    } else {
                        $("#noUsersFoundMessage").remove();
                        userData = '<div class="col" id="noUsersFoundMessage"><p>No users found.</p></div>';
                    }
                    $("#userDataContainer").append(userData);
                }
            });
        });
    });
    </script>
 @endpush

